import os
import sys
import yaml
import json
import tabulate
import requests

USERNAME = "akhilmaskeri@gmail.com"
API_KEY = ""

if len(sys.argv) == 2:
	API_KEY = sys.argv[1]
else:
	print("API KEY not provided") 
	exit(1)

AUTH = (USERNAME, API_KEY)

HEADERS = {
   "Accept": "application/json",
   "Content-Type": "application/json"
}

BASE_URL = "https://amaskeri.atlassian.net"
PAGE_ID = 33011

def pretty_print(json_obj):
	print(json.dumps(json_obj, indent=4))


def get_current_page_version(page_id):

  CONTENT_URL = "{}/wiki/rest/api/content/{}?expand=version".format(BASE_URL, page_id)
  print("connecting to", CONTENT_URL)

  resp = requests.get(CONTENT_URL, auth=AUTH, headers=HEADERS)

  return resp.json()["version"]["number"]


def update_content(page_id, content):

	current_version = get_current_page_version(page_id)

	payload = json.dumps({
		"id": page_id,
		"type": "page",
		"space": {
			"key": "VITAMINC"
		},
		"title": "Auto Generated Document",
		"body": {
			"storage": {
				"value": content,
				"representation": "storage"
			}
		},
		"version": {
			"number": current_version + 1
		}
	})

	CONTENT_URL = "{}/wiki/rest/api/content/{}".format(BASE_URL, page_id)
	resp = requests.put(CONTENT_URL, data=payload, auth=AUTH, headers=HEADERS)

	print(resp.status_code)
	return resp.json()


def read_conf(file_name):

	data = yaml.load(open(file_name), Loader=yaml.SafeLoader)

	value = ["" for i in range(6)]

	if "name" in data and "description" in data:

		value[0] = data["name"]
		value[1] = data["description"]
		
		if "pack" in data:
			value[2] = data["pack"]

		if "entry_point" in data:
			value[3] = data["entry_point"]

		if "runner_type" in data:
			value[4] = data["runner_type"]

		if "enabled" in data:
			value[5] = data["enabled"]

	return value


def get_documents_from_yaml(path):

	result = []
	for dp, _, files in os.walk(path):

		for file_name in files:

			if ".meta." not in file_name:
				continue

			file_format = os.path.splitext(file_name)[1]

			if file_format == ".yaml":
				data = read_conf(os.path.join(dp, file_name))
				if data:
					result.append(data)

	return result

result = get_documents_from_yaml("codebase")


header = ["name", "description", "pack", "entry point", "runner type", "enabled"]
table = tabulate.tabulate(result, header, tablefmt="html")

data = update_content(PAGE_ID, table)
pretty_print(data)
